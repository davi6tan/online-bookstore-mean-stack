# Introduction

MEAN is for building dynamic web sites and web applications. MEAN is a combination of MongoDB, Express.js and Angular.js, all of which run upon Node.js.



### Server-side : Restful API using Node.js, Express.js and MongoDB.
### Client-side:  Using Angular.js.








 
					

## Requirements 

To develop web services for a BookStore using the REST design by implementing the following requirements:

- Buy Book 
    + Include Payment information
    + Include Shipping information 
- Get Order Status Information
- Cancel Book Order 

## The state diagram design

![Stateful diagram for BookStore](https://github.com/davidtanluc/ParallelProgramming/raw/master/wiki1/hateoas1.png)

## Running instructions


- [http://tans-online-store.herokuapp.com/#/books](http://tans-online-store.herokuapp.com/#/books)





---

# API documentation 



## By group (Books)



  `GET "/books/{ISBN}" `
 
 *Function*

 -  returns the search book based on this ISBN.
 
 `GET  "/books"  `

 *Function*

 -  returns all available books in the database, in this case the DAO.
 
 
 
## By group (Orders)

 `GET "/orders" `
 
*Function*

 -  returns all available orders in the database, in this case the DAO.
 
 `GET "/orders/{orderId}/status" `
 
*Function*

 -  returns current orderstatus {PAYMENTPENDING, PAYMENTCOMPLETE, ORDERCANCELLED, SHIPPED, DELIVERED} based on this order Id.

  `GET "/orders/{orderId}" `
 
*Function*

 -  returns order based on this orderId.
 
`POST  "/orders" `

*Function*

 -  creates a new order. 
 -  the following information is required for the order request :
 
     1. items information
     2. customer information with shipping address 
     3. payment information

 `DELETE "/orders/{orderId}" `

*Function*

 -  deletes/cancels order based on this orderId.


