var cartController = function (Cart) {

    ////////  POST /////////
    var post = function (req, res) {
        var cart = new Cart(req.body);
        //console.log("this body ", req.body);
        if (!req.body.name) {
            res.status(400);
            res.send('Title is required');
        }
        else {
            cart.save();
            res.status(201);
            res.send(cart);
        }
    }
    ////////  GET /////////
    var get = function (req, res) {

        var query = {};

        if (req.query.genre) {
            query.genre = req.query.genre;
        }
        Cart.find(query, function (err, carts) {

            if (err)
                res.status(500).send(err);
            else {

                var returnCarts = [];
                carts.forEach(function (element, index, array) {
                    var newCart = element.toJSON();
                    newCart.links = {};
                    newCart.links.self = 'http://' + req.headers.host + '/api/carts/' + newCart._id
                    returnCarts.push(newCart);
                });
                res.json(returnCarts);
            }
        });
    }

    return {
        post: post,
        get: get
    }
}

module.exports = cartController;