var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');


var db;
var uri = 'mongodb://localhost/bookAPI';
db = mongoose.connect(uri);

var Book = require('./Models/bookModel');
var Cart = require('./Models/cartModel');

var app = express();
var port = process.env.PORT || 4000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

bookRouter = require('./Routes/bookRoutes')(Book);
app.use('/api/books', bookRouter);
cartRouter = require('./Routes/cartRoutes')(Cart);
app.use('/api/carts', cartRouter);


app.get('/', function (req, res) {
    res.send('welcome to my API!');
});

app.listen(port, function () {
    console.log('Listening to PORT: ' + port);
});

module.exports = app;