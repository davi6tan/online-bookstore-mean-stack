var express = require('express');


var routes = function (Cart) {
    var cartRouter = express.Router();

    var cartController = require('../Controllers/cartController')(Cart)
    cartRouter.route('/')
        .post(cartController.post)
        .get(cartController.get);

    cartRouter.use('/:cartId', function (req, res, next) {
        Cart.findById(req.params.cartId, function (err, cart) {
            if (err)
                res.status(500).send(err);
            else if (cart) {
                req.cart = cart;
                next();
            }
            else {
                res.status(404).send('no cart found');
            }
        });
    });
    cartRouter.route('/:cartId')
        .get(function (req, res) {

            var returnCart = req.cart.toJSON();

            returnCart.links = {};
            var newLink = 'http://' + req.headers.host + '/api/carts/?genre=' + returnCart.state;
            returnCart.links.FilterByThisGenre = newLink.replace(' ', '%20');
            res.json(returnCart);

        })
        .put(function (req, res) {
            req.cart.name = req.body.name;
            req.cart.payment_type = req.body.payment_type;
            req.cart.payment_info = req.body.payment_info;
            req.cart.street_address = req.body.street_address;
            req.cart.city = req.body.city;
            req.cart.state = req.body.state;
            req.cart.date = req.body.date;
            req.cart.save(function (err) {
                if (err)
                    res.status(500).send(err);
                else {
                    res.json(req.cart);
                }
            });
        })
        .patch(function (req, res) {
            if (req.body._id)
                delete req.body._id;

            for (var p in req.body) {
                req.book[p] = req.body[p];
            }

            req.cart.save(function (err) {
                if (err)
                    res.status(500).send(err);
                else {
                    res.json(req.cart);
                }
            });
        })
        .delete(function (req, res) {
            req.cart.remove(function (err) {
                if (err)
                    res.status(500).send(err);
                else {
                    res.status(204).send('Removed');
                }
            });
        });
    return cartRouter;
};

module.exports = routes;