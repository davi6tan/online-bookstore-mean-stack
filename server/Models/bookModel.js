var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var bookModel = new Schema({
    title: {type: String},
    genre: {type: String},
    author: {type: String},
    price: {type: Number},
    qty: {type: Number},
    ISBN: {type: String},
    read: {type: Boolean, default: false}
});

module.exports = mongoose.model('Book', bookModel);