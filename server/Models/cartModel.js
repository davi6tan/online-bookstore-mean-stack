var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var cartModel = new Schema({
    "name": {type: String},
    "payment_type": {type: String},
    "payment_info": {type: String},
    "street_address": {type: String},
    "city": {type: String},
    "state": {type: String},
    "date": {type: String}
});

module.exports = mongoose.model('carts', cartModel);