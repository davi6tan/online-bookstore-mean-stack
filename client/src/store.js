var app = angular.module('store', ['ngRoute', 'ngAnimate', 'toaster']);

app.config(function ($routeProvider) {
    $routeProvider.when('/books', {templateUrl: 'books.html', controller: 'StoreCtrl'});
    $routeProvider.when('/status', {templateUrl: 'status.html', controller: 'StoreCtrl'});
    $routeProvider.when('/checkout', {templateUrl: 'checkout.html', controller: 'StoreCtrl'});
    $routeProvider.otherwise({redirectTo: '/books'})
});

app.controller('StoreCtrl', function ($scope, $http, $routeParams, $location, toaster) {

    $scope.active = '/books';
    //$scope.$apply();

    //////// STORE //////////////
    var store = function () {
        var books = [];
        var cart = []; // also call order

        var _listBooks = function () {
            return books;
        };
        var count = 0;
        var _createOrder = function (userCart) {
            console.log("I have been clicked create order", userCart, " ", count++);
            //// toaster /////
            $scope.pop = function () {
                toaster.pop('success', "Added books to cart");
            };
            //var placeOrder = angular.copy($scope.order);
            var placeOrder = {};

            placeOrder.bookList = userCart;
            console.log("this place order  booklist", placeOrder.bookList);
            //placeOrder.payment["amount"] = store.totalCartValue().toFixed(2);
            console.log("PLACEorder ", store.totalCartValue().toFixed(2));

            for (var i = 0; i < placeOrder.bookList.length; i++) {
                delete placeOrder.bookList[i].links;
            }

            console.log(placeOrder);
            var placeOrder2 = {
                "name": "Kill Bill3",
                "payment_type": "Credit",
                "payment_info": "total: $128",
                "street_address": "155 Microsoft Road CA",
                "city": "Redwood",
                "state": "CA",
                "date": "09-18-2015"

            };
            if (placeOrder.bookList.length > 0) {
                var urlPost = 'https://tans-chapter-one-server-side.herokuapp.com/api/carts';
                $http.post(urlPost, placeOrder2).success(function (response) {
                    console.log("success POST ", placeOrder2);
                    console.log(response);
                    toaster.pop('success', "Order placed. Shipping in progress.");

                    $scope.orderPlaced = response;
                    console.log("Order placed.");
                    store.clearCartPost();
                    $scope.active = '/books';
                    $location.path('/books');
                });
            } else {
                toaster.error('warning', "There is no order.");

            }
        };

        var _viewCart = function () {
            var userCart = [];
            for (var i = 0; i < cart.length; i++) {
                userCart.push(cart[i]);
            }
            return userCart;
        };

        var _cancelOrder = function (order) {
            console.log("NOT ready yet ", order);
            toaster.pop('warning', "Cancel order.");

        };

        var _checkOrderStatus = function (res) {
            for (var i = 0; i < res.links.length; i++) {
                if (links[i].rel = 'status') {
                    $http.get(links[i].href).success(function (response) {
                        console.log(response);
                        return status;
                    });
                }
            }
            return status;
        }

        var _clearCart = function () {
            cart = [];
            $http.post('/clearCart');
            toaster.pop('wait', "Clear cart.");

        }

        var _clearCartPost = function () {
            cart = [];
            $http.post('/clearCart');
        }
        var _totalCartValue = function () {
            var total = 0;
            for (var i = 0; i < cart.length; i++) {
                total += cart[i].price;
            }
            return total;
        };

        var _addToCart = function (cartOrder) {
            $http.post('/cart', cartOrder).success(function (response) {
                cart = response;
                toaster.pop('success', "Added : " + cartOrder.title + " to cart.");
                delete $scope.cartOrder;
            });
        };

        var _setBooks = function (bookList) {
            books = bookList;
        }

        var _setCart = function (shoppingCart) {
            console.log("htis setcart ", shoppingCart);
            cart = shoppingCart;
        }

        var _orderNow = function (book) {
            for (var i = 0; i < book.links.length; i++) {
                if (book.links[i].rel == "order") {

                }
            }
        }

        var _getDescription = function (link) {
            var linkz = angular.copy(link);
            if (linkz.rel == "description") {
                $http.get(linkz.href).success(function (response) {
                    return response.description;
                });
            }
        }

        var _doLogin = function () {
            var userLogin = angular.copy($scope.login);
            $http.get('https://tans-chapter-one-server-side.herokuapp.com/api/customers/' + userLogin).success(
                function (response) {
                    console.log(response);
                    for (var i = 0; i < response.links.length; i++) {
                        if (response.links[i].rel == "orders") {
                            $http.get(response.links[i].href).success(function (response) {
                                $scope["userOrders"] = response;
                            });
                        }
                    }
                });
        }

        var _getUserOrders = function () {
            console.log("STORE USER undefined ", $scope.userOrders);
            return $scope.userOrders;
        }

        return {
            listBooks: _listBooks,
            createOrder: _createOrder,
            viewCart: _viewCart,
            cancelOrder: _cancelOrder,
            totalCartValue: _totalCartValue,
            addToCart: _addToCart,
            setBooks: _setBooks,
            setCart: _setCart,
            clearCart: _clearCart,
            clearCartPost: _clearCartPost,
            orderNow: _orderNow,
            getDescription: _getDescription,
            //checkOrderStatus: _checkOrderStatus,
            doLogin: _doLogin,
            getUserOrders: _getUserOrders
        }

    }(); // #END store

    ///// HELPER FUNCTIONS //////
    $scope.loading = true;
    $scope.store = store;
    var loadBooks = function () {
        $scope.loading = true;
        $http.get('https://tans-chapter-one-server-side.herokuapp.com/api/books').success(function (books) {
            store.setBooks(books);
            $scope.loading = false;
        });
    }; //loadBooks

    var loadCart = function () {
        $http.get('/cart').success(function (response) {
            store.setCart(response);
        });
    }; //loadCart

    loadBooks();
    loadCart();
});