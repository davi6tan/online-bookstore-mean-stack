var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser());
app.use(express.static('src/'));

var shoppingCart = [];

app.get('/cart', function (req, res) {
    res.set({'content-type': 'application/json; charset=utf-8'});
    res.json(shoppingCart);
    console.log("Shopping cart GET : " + shoppingCart.length, " ", JSON.stringify(shoppingCart));
    console.log('Returning cart with orders...')
});

app.post('/cart', function (req, res) {
    var cartOrder = req.body;
    shoppingCart.push(cartOrder);
    console.log("Shopping cart POST : " + shoppingCart, " ", cartOrder);
    res.json(shoppingCart);
    console.log('Returning cart with orders...');
});

app.post('/clearCart', function (req, res) {
    shoppingCart = [];
    res.json(shoppingCart);
    console.log("Cart empty.");
});

app.listen(process.env.PORT || 3000);
console.log('Listening port 3000...')